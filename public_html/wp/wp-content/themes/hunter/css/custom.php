<?php
header("Content-type: text/css;");

$cache = 'cached.css';

if( file_exists($cache) && filemtime( $cache ) < ( time() + ( 60 * 60 * 1000 ) ) ){
  include $cache;
}else{
	
	if( file_exists('../../../../wp-load.php') ) :
		include '../../../../wp-load.php';
	else:
		include '../../../../../wp-load.php';
	endif;

	// Styles	
	$primary 	= ft_of_get_option('fabthemes_primary_color','#769A38');
	$secondary	= ft_of_get_option('fabthemes_secondary_color','');
	$link 		= ft_of_get_option('fabthemes_link_color','');
	$hover 		= ft_of_get_option('fabthemes_hover_color','');
	$header		= ft_of_get_option('fabthemes_headerpic','');


  $css = sprintf("#masthead{ background-image:url(%s);}

    .search-box, .audio-format .audioplayer, .audio-format .audioplayer .mejs-controls,
    .link-format, .quote-format, #comments h2.comments-title span, #comments #respond p.form-submit input{
     background: %s!important;
       }
    #comments #respond p.form-submit input{
       border-color:%s!important;
     }
    #comments ol.comment-list li .comment-body .comment-meta .comment-metadata a.comment-reply-link{
     color: %s!important;
    }

    .main-navigation{
    	background: %s;
    }
    /* draw any selected text yellow on red background */
    ::-moz-selection { color: #fff;  background: #222; }
    ::selection      { color: #fff;  background: #222; } 
    /* Links */

    a, .hentry .entry-header .entry-meta span {
    	color: %s;
    }

    a:visited {
    	color: %s;
    }

    a:hover,
    a:focus,
    a:active {
    	color:%s;
    	text-decoration: none;
    }", $header, $primary, $primary, $primary, $secondary, $link, $link, $hover);


  file_put_contents($cache, $css);
}


echo $css;